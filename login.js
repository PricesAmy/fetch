/*
 * @Author: shihong.lei
 * @Date: 2018-10-01 16:09:19
 * @Last Modified by:   shihong.lei
 * @Last Modified time: 2018-10-01 16:09:19
 */
// 如登录界面的用到的api, 统一管理在login / api / index.js中，如下：
import fetch from "@/utils/fetch";

/**
* 登录
*/
export const login = (reqData) => fetch("/v2/cotton/user/app_login", reqData);
